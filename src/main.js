import Vue from 'vue'
import App from './App.vue'
import RecipeList from './components/RecipeList.vue'
import RecipeDetail from './components/RecipeDetail.vue'
import VueRouter from 'vue-router'

Vue.config.productionTip = false
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  props: true,
  routes: [
    {
      path: '/',
      name: 'recipes',
      component: RecipeList
    },
    {
      path: '/:slug/:id',
      name: 'recipe',
      component: RecipeDetail,
      props: true
    }
  ]
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
